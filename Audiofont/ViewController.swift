//
//  ViewController.swift
//  Audiofont
//
//  Created by aditya.kalamkar on 27/08/19.
//  Copyright © 2019 aditya.kalamkar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var enterTextLabel: UILabel!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var sourceUrlLabel: UILabel!
    @IBOutlet weak var sourceUrlTextField: UITextField!
    @IBOutlet weak var selectGenderLabel: UILabel!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var maleRadioLabel: UILabel!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioLabel: UILabel!
    @IBOutlet weak var speechSpeedLabel: UILabel!
    @IBOutlet weak var speechSpeedSlider: UISlider!
    @IBOutlet weak var speedCountLabel: UILabel!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Intialization of UI
        self.inputTextView.layer.borderColor = UIColor.black.cgColor
        self.inputTextView.layer.borderWidth = 1.0
        self.inputTextView.layer.cornerRadius = 5.0
        self.speedCountLabel.backgroundColor = UIColor.lightGray
        self.speedCountLabel.layer.cornerRadius = 5.0
        self.speedCountLabel.clipsToBounds = true
        self.playButton.layer.cornerRadius = 5
        self.playButton.layer.borderWidth = 1
        self.playButton.backgroundColor = UIColor.buttonBackgroundGreen
        self.playButton.setTitleColor(UIColor.white, for: .normal)
        self.playButton.layer.borderColor = UIColor.black.cgColor
        self.downloadButton.layer.cornerRadius = 5
        self.downloadButton.layer.borderWidth = 1
        self.downloadButton.backgroundColor = UIColor.buttonBackgroundGreen
        self.downloadButton.setTitleColor(UIColor.white, for: .normal)
        self.downloadButton.layer.borderColor = UIColor.black.cgColor
        
        //Setting Gesture for radioButtons
        let tapmaleLabel = UITapGestureRecognizer(target: self, action: #selector(maleRadioButtonClicked(_:)))
        self.maleRadioLabel.isUserInteractionEnabled = true
        self.maleRadioLabel.addGestureRecognizer(tapmaleLabel)
        let tapfemaleLabel = UITapGestureRecognizer(target: self, action: #selector(femaleRadioButtonClicked(_:)))
        self.femaleRadioLabel.isUserInteractionEnabled = true
        self.femaleRadioLabel.addGestureRecognizer(tapfemaleLabel)
        
    }

    @IBAction func playButtonClicked(_ sender: Any) {
    }
    
    @IBAction func downloadButtonClicked(_ sender: Any) {
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
    }
    
    @IBAction func maleRadioButtonClicked(_ sender: Any) {
        
        self.maleRadioButton.setImage(UIImage(named: "checked"), for: .normal)
        self.femaleRadioButton.setImage(UIImage(named: "unchecked"), for: .normal)
    }
    
    @IBAction func femaleRadioButtonClicked(_ sender: Any) {
        self.maleRadioButton.setImage(UIImage(named: "unchecked"), for: .normal)
        self.femaleRadioButton.setImage(UIImage(named: "checked"), for: .normal)
    }
    
    @IBAction func speechSliderValueChanged(_ sender: Any) {
        self.speedCountLabel.text = String(format: "%i",Int(self.speechSpeedSlider.value))
    }
}

